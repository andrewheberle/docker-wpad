# docker-wpad #

[![build status](https://gitlab.com/andrewheberle/docker-wpad/badges/master/build.svg)](https://gitlab.com/andrewheberle/docker-wpad/commits/master)

## Description ##

Small web server to server WPAD files derived from "gitlab.com/andrewheberle/docker-base-confd"

