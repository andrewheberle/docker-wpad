FROM registry.gitlab.com/andrewheberle/docker-base:2.2

# Container environment
ENV DOMAIN_NAME=example.net \
    PROXY_HOSTNAME=web-proxy \
    PROXY_PORT=3128

# Copy root fs skeleton
COPY root /

EXPOSE 80/tcp

# Check HTTP response is ok
HEALTHCHECK CMD wget -q -S -O /dev/null http://localhost/wpad.dat 2>&1 | head -1 | grep -q "HTTP/1.1 200"

ARG VERSION
ARG NAME="docker-wpad"
ARG DESCRIPTION="Container to serve WPAD proxy autoconfig"
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL="https://gitlab.com/andrewheberle/$NAME"

LABEL name="$NAME" \
      version="$VERSION" \
      architecture="amd64" \
      description="$DESCRIPTION" \
      maintainer="Andrew Heberle ($VCS_URL)" \
      org.label-schema.description="$DESCRIPTION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="$NAME" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="$VCS_URL" \
      org.label-schema.version="$VERSION" \
      org.label-schema.schema-version="1.0"
